import { Radio, Spin, Switch, Row, Col, Button } from 'antd';
import React, { useEffect, useState } from 'react';
import Chart from './Chart';
import Icon from "./icon.jpg";
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';

import { callData } from '../functions/index.js';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Chip from '@material-ui/core/Chip';
const { Option } = Select;
const ChartComponent = () => {
	const [chart, setChart] = useState('line');
	const [data, setData] = useState(null);
	const [size, setSize] = useState([]);
	const [method, setMethod] = useState(['close']);
	const [model, setModel] = useState('lstm');
	const [tooltip, setTooltip] = useState(false);
	const [loading, setLoading] = useState(true);
	const [stock, setStock] = useState("aapl");
	useEffect(() => {

		const callDataStock = async () => {
			setLoading(true);
			const res = await callData(stock, method, model).catch(err => {
				setLoading(false);
			});
			const data = res?.data
			if (data) {
				let tomorrowDate = new Date();
				tomorrowDate.setDate(tomorrowDate.getDate() + 1);
				const tomorrow = {
					close: null,
					predict: data.tomorrow,
					date: tomorrowDate
				}
				const newData = await new Promise((resolve, reject) => resolve(data.csv.map(value =>
				({
					...value,
					date: new Date(value.date)
				}))));
				newData.push(tomorrow);
				if (newData) {
					setData(newData);
					setLoading(false);
				}
			}

		}
		callDataStock();
	}, [stock, method, model])



	const handleChangeStock = (event) => {
		setStock(event.target.value);
	}

	// const handleSizeChange = e => {
	// 	setChart(e.target.value);
	// };

	return (
		<>

			<Spin size="large" spinning={loading}>
				<div className="body">
					<div className="header">
						<h3 style={{
							display: "flex", marginLeft: 30, borderBottom: "1px solid #ccc",
							padding: "5px", color: "black", alignItems: "center",
							justifyContent: "space-between"
						}}>
							<span>
								ĐỒ ÁN CUỐI KÌ
							</span>
						</h3>

					</div>
					<div >


						<Row>
							<Col span={6}>

								<InputLabel id="demo-simple-select-label">STOCK</InputLabel>


								<Select
									showSearch
									style={{ width: 95 }}
									defaultValue="AAPL"
									optionFilterProp="children"
									onChange={handleChangeStock}

								>
									<MenuItem value="CSGP">CSGP</MenuItem>
									<MenuItem value="AAPL">AAPL</MenuItem>
									<MenuItem value="MSFT">MSFT</MenuItem>
									<MenuItem value="AMZN">AMZN</MenuItem>
									<MenuItem value="GOOG">GOOG</MenuItem>
									<MenuItem value="GOOGL">GOOGL</MenuItem>
									<MenuItem value="FB">FB</MenuItem>
									<MenuItem value="TSLA">TSLA</MenuItem>
									<MenuItem value="TSM">TSM</MenuItem>
									<MenuItem value="BABA">BABA</MenuItem>
									<MenuItem value="V">V</MenuItem>
									<MenuItem value="NVDA">NVDA</MenuItem>
									<MenuItem value="JPM">JPM</MenuItem>
									<MenuItem value="JNJ">JNJ</MenuItem>
									<MenuItem value="WMT">WMT</MenuItem>
									<MenuItem value="UNH">UNH</MenuItem>
									<MenuItem value="MA">MA</MenuItem>
									<MenuItem value="PYPL">PYPL</MenuItem>
									<MenuItem value="BAC">BAC</MenuItem>
									<MenuItem value="HD">HD</MenuItem>
									<MenuItem value="PG">PG</MenuItem>
									<MenuItem value="DIS">DIS</MenuItem>
									<MenuItem value="ADBE">ADBE</MenuItem>
									<MenuItem value="ASML">ASML</MenuItem>
									<MenuItem value="CMCSA">CMCSA</MenuItem>
								</Select>
							</Col>



							<Col span={6}>
								<InputLabel id="demo-simple-select-label">MODEL</InputLabel>

								<Select
									showSearch
									style={{ width: 150 }}
									defaultValue="LSTM"
									optionFilterProp="children"
									onChange={(event) => setModel(event.target.value)}
									filterOption={(input, option) =>
										option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
									}
								>
									<MenuItem value="lstm">LSTM</MenuItem>
									<MenuItem value="xgb">XGBoost</MenuItem>
									<MenuItem value="rnn">RNN</MenuItem>
								</Select>
							</Col>

							<Col span={6}>
								<InputLabel id="demo-mutiple-checkbox-label">FEATURE</InputLabel>


								<Select
									labelId="demo-mutiple-checkbox-label"
									id="demo-mutiple-checkbox"
									multiple
									defaultValue={["close"]}
									onChange={(event) => {
										if (event.target.value.length < 1) {
											event.target.value.push("close")
										}
										setMethod(event.target.value);
									}}
									input={<Input />}
									renderValue={(selected) => selected.join(', ')}
								>
									<MenuItem key="close" value='close'>
										<Checkbox checked={method.indexOf('close') > -1} />
										Close</MenuItem>
									<MenuItem key="poc" value='poc'>
										<Checkbox checked={method.indexOf('poc') > -1} />
										Price Of Change</MenuItem>
								
								</Select>
							</Col>
						</Row>


						{/* <div className="label">
							FEATURE
						</div>
						<div>
							<Select
								multiple
								placeholder="Indicator"
								onChange={(event) => {
									if (event.target.value.length < 1) {
										event.target.value.push("close")
									}
									setMethod(event.target.value);
								}}
								defaultValue={["close"]}
								style={{ width: '100%', minWidth: "120" }}
							>
								<MenuItem key="close">Close</MenuItem>
								<MenuItem key="poc">Price Of Change</MenuItem>
								<MenuItem key="rsi">RSI</MenuItem>
								<MenuItem key="bb">Bollinger Bands</MenuItem>
							</Select>

						</div> */}
					</div>
					{
						data ? <Chart
							line='line'
							BBand={size.includes("BBand")}
							data={data} /> : <></>
					}
				</div>
			</Spin>
		</>
	)
}

export default ChartComponent;